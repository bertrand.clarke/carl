#!/bin/bash
for PROGRAM in Serveur carl.sh
do
	PS=`ps -ef | grep $PROGRAM| grep -v grep | awk '{print $2}'`
	if [ "$PS" != "" ]
	then	
		kill $PS
	fi
done
