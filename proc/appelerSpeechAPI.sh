#!/bin/bash

RESULTAT_HTML=`curl -X POST --data-binary @/tmp/son.wav --header 'Content-Type: audio/l16; rate=44100;' "https://www.google.com/speech-api/v2/recognize?output=json&lang=fr&key=$CARL_GOOGLE_SPEECH_API_KEY" 2> /tmp/curl.log`
date >/tmp/appelerSpeechAPI.log
ERROR=`echo $RESULTAT_HTML | grep "Your client does not have permission to get URL"`
if [ "$ERROR" != "" ]
then
	exit 1
fi
echo $RESULTAT_HTML >> /tmp/appelerSpeechAPI.log
RESULTAT=`echo $RESULTAT_HTML | sed -e 's/[{}]/''/g' | awk -F":" '{print $5}' | awk -F"," '{print $1}' | sed 's/"//g'`
echo $RESULTAT
