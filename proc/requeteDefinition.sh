#!/bin/bash
LOG=/tmp/`basename $0`.log
#TEXT=`curl http://www.le-dictionnaire.com/definition.php?mot=$1 2>$LOG | xmllint --html --xpath '//span[text()="Définition du mot :"]/..' - 2> $LOG | html2text | sed -e 's/\[images.*\]//g' -e 's/\[images\///' -e 's/puce.gif\]//' | tr '\n' ' '`

#TEXT=`curl http://dictionnaire.reverso.net/francais-definition/$1 2>$LOG| xmllint --html --xpath '//div[@id="ctl00_cC_translate_box"]//span[@direction="target"]' - 2>$LOG| html2text -utf8`

curl http://fr.wiktionary.org/wiki/$1 | xmllint --html --xpath '//ol' - | html2text -utf8 | sed 's/_/ /g'
