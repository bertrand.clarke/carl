#!/bin/bash
if [ -z $CARL_PROFIL ]
then
	echo CARL_PROFIL non défini Exemple: ~/.carl
	exit 1
fi

if [ -f $CARL_PROFIL ]
then
	INDEX_TEXT=$(cat $CARL_PROFIL | grep INDEX_TEXT)
	if [ "$INDEX_TEXT" = "" ]
	then
		echo "INDEX_TEXT=0" >> $CARL_PROFIL
	fi
else
	INDEX_TEXT=0
	echo "INDEX_TEXT=0" > $CARL_PROFIL
fi

((++INDEX_TEXT))
cat $CARL_PROFIL | sed "s/INDEX_TEXT=.*/INDEX_TEXT=$INDEX_TEXT/" > /tmp/.carl
mv /tmp/.carl $CARL_PROFIL
SON=`curl http://radiofrance-podcast.net/podcast09/rss_11466.xml |xmllint --xpath '//guid' - | sed -e 's%</guid><guid>%\n%g' | sed -e 's%<guid>%%g' -e 's%</guid>%\n%g' | head -$INDEX_TEXT | tail -1`
mplayer $SON
