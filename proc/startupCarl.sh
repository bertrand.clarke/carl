#!/bin/bash

### BEGIN INIT INFO
# Provides:          carl
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO

case "$1" in 
    start)
        echo "Starting carl"
	su - pi /home/pi/carl/proc/startCarl.sh
        ;;
    stop)
        echo "Stopping carl"
        su - pi /home/pi/carl/proc/stopCarl.sh
        ;;
    *)
        echo "Usage: /etc/init.d/startupCarl.sh start|stop"
        exit 1
        ;;
esac

exit 0
