#!/bin/bash
LOG=/tmp/carl.log
nohup java -cp $CARL_HOME/target/carl-1.0-jar-with-dependencies.jar Serveur >$LOG 2>&1 &
nohup carl.sh $* >>$LOG 2>&1 &
echo "Carl started" >>$LOG
