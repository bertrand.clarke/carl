#!/bin/bash
TEXT=`echo $1 | sed "s/[|&[(){}]//g" | sed "s/]//g"`
#Utiliser dernière version de espeak $CMD_IN | sed -e 's/\<les\>/lai/g' -e 's/\<des\>/dai/g'| espeak --stdin -v mb-fr4 -q --pho -s 90 | mbrola -e /opt/mbrola/fr4 - - | aplay -r25000 -fS16
pico2wave --lang=fr-FR --wave=/tmp/$$.wav "$TEXT"
mplayer /tmp/$$.wav >/tmp/dire.log 2>&1; rm /tmp/$$.wav
