#!/bin/bash
LOG=/tmp/`basename $0`.log
curl http://fr.wiktionary.org/wiki/$1 2>$LOG | xmllint --html --xpath '//dd' - 2>$LOG| head -1 | sed 's/<\/dd>.*//' | sed 's/<dd>//' | html2text -utf8
