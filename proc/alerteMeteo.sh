#!/bin/bash
LOG=/tmp/meteo.log
echo VigilanceMeteo >$LOG
RESULTAT=`curl http://$CARL_HOST/meteo?departement=31 2>$LOG`
echo $RESULTAT >> $LOG
if [ "$RESULTAT" != "" ]
then
        dire.sh "Vigilance météo pour la Haute-Garonne : $RESULTAT"
fi
echo VigilanceCrue >>$LOG
RESULTAT=`curl http://www.vigicrues.gouv.fr/niv_spc.php?idspc=25 2>$LOG| xmllint --html --xpath '//p[text()="Arrats - Gimone - Save - Touch"]/../../td[2]' - 2>$LOG| html2text | head -1`
echo "Vigilance crue : ${RESULTAT}" >> $LOG
if [ "$RESULTAT" != "Vert" ]
then
        dire.sh "Attention, vigilance crue de niveau $RESULTAT sur le Touche"
fi

