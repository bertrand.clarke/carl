#!/bin/bash
DURATION=10
THRESHOLD=0.25

function ecouter
{
	arecord -D plughw:1,0 -f cd -t wav -d $DURATION -r 16000 >/tmp/$$.wav 2>/dev/null
	MAXIMUM_AMPLITUDE=`sox /tmp/$$.wav /tmp/$$_out.wav stat 2>&1 | grep "Maximum amplitude" | awk '{print $3}'`
	echo $MAXIMUM_AMPLITUDE'>'$THRESHOLD
	PAROLE_ENTENDUE=`echo $MAXIMUM_AMPLITUDE'>'$THRESHOLD | bc -l`
}

while :
do
	ecouter
	if [ "$PAROLE_ENTENDUE" == "1" ]
	then
		echo "Son entendu /tmp/$$.wav"	
		curl "https://smsapi.free-mobile.fr/sendmsg?$SMS_PARAMS&msg=Alerte%20son%20dans%20la%20maison"
		mpack -s "Alarme sonore du raspberry" /tmp/$$.wav bertrand.clarke@gmail.com
	fi
	rm -f /tmp/$$.wav
done
