#!/bin/bash
TEXT=`echo $* | sed "s/[|&[(){}]//g" | sed "s/]//g"`
pico2wave --lang=en-GB --wave=/tmp/$$.wav "$TEXT" ; mplayer /tmp/$$.wav >/dev/null 2>&1; rm /tmp/$$.wav

