#!/bin/bash
DEBUG=$1

function urlencode 
{
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c"
        esac
    done
}

function ecouter
{
	logDebug "Début écoute"
	#rec -q --encoding signed-integer --channels 1 --rate 44100 /tmp/son.wav silence 1 0.1 3% 1 3.0 3%
	rec -q --encoding signed-integer --channels 1 --rate 44100 /tmp/son.wav &
	REC=$!
	sleep 5
	logDebug "Fin écoute"
	kill $REC >/dev/null
}

function detecterParole
{
	arecord -D plughw:1,0 -f cd -t wav -d 1 -r 16000 /tmp/detection.wav >/dev/null 2>/tmp/detection.log
	STATS=`sox /tmp/detection.wav /tmp/sox_out.flac stat 2>&1`
	MAXIMUM_AMPLITUDE=`echo $STATS| awk '{print $12}'`
	#logDebug MAXIMUM_AMPLITUDE=$MAXIMUM_AMPLITUDE
	THRESHOLD=0.9
	PAROLE_ENTENDUE=`echo $MAXIMUM_AMPLITUDE'>'$THRESHOLD | bc -l`
}

function analyse
{
	echo "$1" | grep -i "$2" >/dev/null
}

function erreurSpeechAPI
{
	dire "Erreur"
}

function traduire
{
	LANG=`echo $TEXT | awk '{print $1}'`
	MOT=`echo $TEXT | awk '{print $2}'`
	if [ "$MOT" == "" ]
	then
		dire "Répètes le mot à traduire"
		ecouter
		if [ "$PAROLE_ENTENDUE" == "1" ]
		then
			utiliserSpeechAPI
			if [ $? -ne 0 ]
			then
				return
			fi
			MOT=$TEXT
		else
			dire "Je n'ai pas entendu"
		fi
	fi
	dire "Traduction $LANG de $MOT" &
	TRADUCTION=`curl "http://$CARL_HOST/traduction?clientId=$(urlencode $CARL_MS_CLIENT_ID)&clientSecret=$(urlencode $CARL_MS_CLIENT_SECRET)&fromLanguage=fr&toLanguage=$LANG&text=$(urlencode "$MOT")"`
	echo Traduction : $TRADUCTION
	echo dire_$LANG.sh "$TRADUCTION"
	dire_$LANG.sh "$TRADUCTION"
}

function utiliserSpeechAPI
{
	TEXT=`appelerSpeechAPI.sh`
	if [ $? -ne 0 ]
	then
		erreurSpeechAPI
		return 1
	elif [ "$TEXT" != "" ]
	then
		echo Interpretation : $TEXT
	fi
	return 0
}

function jouerMusique {
	echo mplayer -quiet $MUSIQUE
	mplayer -quiet $MUSIQUE >/dev/null &
	PROC_ID=$!
}

function chercherMusique {
	rm -f /tmp/musique
	dire "Je recherche $TEXT"
	YOUTUBE=`curl -s "http://$CARL_HOST/youtube?apiKey=$CARL_GOOGLE_SPEECH_API_KEY&requete=$(urlencode "$TEXT")"`
	URL_YOUTUBE=`echo $YOUTUBE | xmllint --xpath '//videoId/text()' -` 
	TITLE_YOUTUBE=`echo $YOUTUBE | xmllint --xpath '//title/text()' -` 
	dire "J'ai trouvé : $TITLE_YOUTUBE" &
	echo $URL_YOUTUBE $TITLE_YOUTUBE
	MUSIC_FILE="~/Music/"`echo ${TEXT}_${TITLE_YOUTUBE} | sed -e 's/[^A-Za-z0-9._-]/_/g'`".mp4"
	youtube-dl -q -f worst -o /tmp/musique $URL_YOUTUBE &
	PROC_ID_EN_ATTENTE=$!
	MUSIQUE=/tmp/musique
	ACTION_EN_ATTENTE=jouerMusique
}

function chercherPlaylist {
	rm -f /tmp/musique
	dire "Je recherche $TEXT"
	YOUTUBE=`curl "https://www.googleapis.com/youtube/v3/search?part=snippet&q=$(urlencode "$TEXT")&key=$CARL_GOOGLE_SPEECH_API_KEY"`
	// TODO : parser json
	VIDEO_ID=`echo $YOUTUBE | xmllint --xpath '//videoId/text()' -` 
	TITLE_YOUTUBE=`echo $YOUTUBE | xmllint --xpath '//title/text()' -` 
	dire "J'ai trouvé : $TITLE_YOUTUBE" &
	youtube-dl -f worst -o /tmp/musique http://www.youtube.com/watch?v=$VIDEO_ID &
	PROC_ID_EN_ATTENTE=$!
	MUSIQUE=/tmp/musique
	ACTION_EN_ATTENTE=jouerMusique
}

function jouerFranceInter
{
	MUSIQUE=http://audio.scdn.arkena.com/11008/franceinter-midfi128.mp3
	jouerMusique
	PROC_ID=$!
}

function arreter
{
	logDebug "Traitement arrêt"
	if [ "$PROC_ID_EN_ATTENTE" != "" ]
	then
		dire "J'annule la demande"
		kill ${PROC_ID_EN_ATTENTE}
		PROC_ID_EN_ATTENTE=""
		ACTION_EN_ATTENTE=""
	fi
	if [ "$PROC_ID" != "" ]
	then
		kill $PROC_ID
		PROC_ID=""
	fi
}

function dire
{
	dire.sh "$1"
}

function interpreter
{
	utiliserSpeechAPI
	if [ $? -ne 0 ]
	then
		return
	fi
	if analyse "$TEXT" "France Inter"; 
	then
		arreter
		dire "Je met France Inter"
		jouerFranceInter
	elif analyse "$TEXT" "radio";
	then
		arreter
		#mplayer http://live.streamhosting.ch:8010&
		#MUSIQUE=http://diffusion.lafrap.fr/alternantes.mp3
		MUSIQUE=http://audio.scdn.arkena.com/11016/fip-midfi128.mp3
		jouerMusique
		PROC_ID=$!	
	elif analyse "$TEXT" "alternative";
	then
		arreter
		MUSIQUE=http://stream1.addictradio.net/addictalternative.mp3
		jouerMusique
		PROC_ID=$!	
	elif analyse "$TEXT" "lounge";
	then
		arreter
		MUSIQUE=http://stream1.addictradio.net/addictlounge.mp3
		jouerMusique
		PROC_ID=$!	
	elif analyse "$TEXT" "rock";
	then
		arreter
		MUSIQUE=http://stream1.addictradio.net/addictrock.mp3
		jouerMusique
		PROC_ID=$!	
	elif analyse "$TEXT" "stop\|arrêt";
	then
		arreter
	elif analyse "$TEXT" "définition";
	then
		arreter
		MOT=`echo $TEXT | awk '{print $2}'`
		if [ "$MOT" == "" ]
		then
			dire "Répètes le mot ?"
			ecouter
			if [ "$PAROLE_ENTENDUE" == "1" ]
			then
				utiliserSpeechAPI
				if [ $? -ne 0 ]
				then
					return
				fi
			else
				dire "Je n'ai pas entendu"
			fi
		fi
		dire "Définition de $MOT"&	
		DEFINITION=`requeteDefinition.sh "$MOT"`
		echo $DEFINITION
		dire "$DEFINITION" &
		PROC_ID=$!
	elif analyse "$TEXT" "étymologie";
	then
		arreter
		MOT=`echo $TEXT | awk '{print $2}'`
		if [ "$MOT" == "" ]
		then
			dire "Etymologie de quel mot ?"
			ecouter
			if [ "$PAROLE_ENTENDUE" == "1" ]
			then
				utiliserSpeechAPI
				if [ $? -ne 0 ]
				then
					return
				fi
			else
				dire "Je n'ai pas entendu"
			fi
		fi
		dire "Etymologie de $MOT"&	
		ETYMOLOGIE=`requeteEtymologie.sh "$MOT"`
		dire "$ETYMOLOGIE" & 
		PROC_ID=$!
	elif analyse "$TEXT" "espagnol";
	then
		arreter
    		traduire
	elif analyse "$TEXT" "anglais";
	then
		arreter
		traduire
	elif analyse "$TEXT" "musique";
	then
		arreter
		chercherMusique
	elif analyse "$TEXT" "garder";
	then
		echo cp /tmp/musique $MUSIC_FILE
		cp /tmp/musique $MUSIC_FILE
	elif analyse "$TEXT" "playlist";
	then
		arreter
		chercherPlaylist
	elif analyse "$TEXT" "youtube";
	then
		arreter
		TEXT=`echo $TEXT | sed 's/youtube//'`
		chercherMusique
	elif analyse "$TEXT" "alarme";
	then
		arreter
		dire "Je met l'alarme"
		alarme.sh
		exit
	elif analyse "$TEXT" "aide";
	then
		arreter
		dire "Je comprends musique radio France Inter alternative lounge rock définition étymologie espagnol anglais garder alarme"
		alarme.sh
		exit
	elif [ "$TEXT" != "" ]
	then
		echo "J'ai compris : $TEXT"
		if [ "$PROC_ID" == "" ]
		then
			dire "J'ai compris : $TEXT"
		fi
	fi
}

function logDebug 
{
	if [ "$DEBUG" != "" ]
        then
        	echo $1
        fi
}

date
echo "Starting carl"
logDebug DEBUG=$DEBUG
while :
do
	if [ "$PROC_ID_EN_ATTENTE" != "" ] && [ ! -d /proc/$PROC_ID_EN_ATTENTE ]
	then
		logDebug "Déclenchement de l'action en attente"
		PROC_ID_EN_ATTENTE=""
		$ACTION_EN_ATTENTE
		ACTION_EN_ATTENTE=""
	fi
	detecterParole
	if [ "$PAROLE_ENTENDUE" == "1" ]
	then
		logDebug "Son entendue"
		if [ "$PROC_ID" == "" ]
		then
			dire "oui ?"
		fi
		ecouter
		if [ "$PAROLE_ENTENDUE" == "1" ]
		then
			logDebug "Parole entendue"
			interpreter
		fi
	fi
	JOUR=`date +%A`
	if [ "$JOUR" == "samedi" ] || [ "$JOUR" == "dimanche" ]
	then
		REVEIL_HEURES="09"
		REVEIL_MINUTES="00"
	else
		REVEIL_HEURES="06"
		REVEIL_MINUTES="58"
	fi
	HEURE=`date +%H%M`
	if [ "$PROC_ID" == "" ] && [ "$HEURE" == "$REVEIL_HEURES$REVEIL_MINUTES" ]
	then
		dire "C'est l'heure de se lever."
		alerteMeteo.sh
		#texteDuJour.sh
		#jouerFranceInter
		sleep 60
	fi
	# Suppression de PROC_ID si le processus n'existe plus
	if [ "$PROC_ID" != "" ] && [ ! -d /proc/$PROC_ID ]
	then
		PROC_ID=""
	fi
done
