#Matériel
* Rasberry pi B+

## Entrée son
* Carte son C-Media Electronics, Inc. avec un micro de PC

## Sortie son
DAC+ https://www.hifiberry.com/dacplus relié à la chaîne hifi

#Système
Installer via https://www.hifiberry.com/guides/hifiberry-installer/ pour avoir le bon paramétrage pour le Hifiberry DAC+

ssh pi@<ip du raspberry pi>
Mot de passe : raspberry

```
sudo raspi-config
```
* Expand Filesystem
* Change User Password
* Internationalisation Options / Change locale (Ajout de fr_FR.UTF-8)
* Internationalisation Options / Change Timezone

##Installation des packages
```
sudo apt-get update
sudo apt-get install automake libtool libpopt-dev libxml2-utils flac sox mplayer2 bc html2text youtube-dl ssmtp mailutils mpack git
```
Suite à ERROR: unable to download video de youtube-dl :
```
sudo pip install --upgrade youtube_dl
```
##Récupération des sources
```
git clone https://github.com/BertrandClarke/carl
```
##Environnement
##Ajouter au fichier ~/.bash_profile :
```
export CARL_GOOGLE_SPEECH_API_KEY=xxx
export CARL_LANGUAGE_CLOUD_API_KEY=xxxx
export CARL_MS_CLIENT_ID=CARL_RASPI
export CARL_MS_CLIENT_SECRET=xxxxx
export CARL_PROFIL=~/.carl
export CARL_HOST=localhost:8088
export SMS_PARAMS="user=xxxxxx&pass=xxxxx"

export AUDIODEV=hw:1,0
export CARL_HOME=~/carl
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias h='history'
alias psCarl='ps -ef | grep -e Serveur -e carl.sh | grep -v grep | grep -v vi'
alias tailCarl='tail -f /tmp/carl.log'
alias cdCarl='cd $CARL_HOME/proc'
export VISUAL=vim
export EDITOR="$VISUAL"
export PATH=$PATH:$CARL_HOME/proc:$HOME/bin
export JAVA_HOME=/etc/alternatives
```

##Démarrage automatique
```
cd /etc/init.d
sudo ln -s /home/pi/carl/proc/startupCarl.sh .
sudo update-rc.d startupCarl.sh defaults
```

####Entrée
Dans /etc/modprobe.d/alsa-base.conf, commenter la ligne 
```
options snd-usb-audio index=-2
```

Régler le gain du micro à 94%
```
alsamixer
sudo alsactl store
```

Test du son :
```
~/carl/proc/testSon.sh
```

###Logiciels
####Synthèse vocale avec svxo-pico
Activer le repository des sources dans /etc/apt/sources.list :
```
deb-src http://archive.raspbian.org/raspbian/ jessie main contrib non-free rpi
deb http://ftp.de.debian.org/debian jessie main non-free
```
```
sudo apt-get update
sudo apt-get install fakeroot debhelper automake autoconf libtool help2man libpopt-dev hardening-wrapper
mkdir pico_build
cd pico_build
apt-get source libttspico-utils
cd svox-1.0+git20130326-3
dpkg-buildpackage -rfakeroot -us -uc -d
cd ..
sudo dpkg -i libttspico-data_1.0+git20130326-3_all.deb
sudo dpkg -i libttspico0_1.0+git20130326-3_armhf.deb
sudo dpkg -i libttspico-utils_1.0+git20130326-3_armhf.deb
```
