import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

public class RechercheYoutube extends HttpServlet {

	private static final long NUMBER_OF_VIDEOS_RETURNED = 1;

	/**
	 * Define a global instance of the HTTP transport.
	 */
	public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	/**
	 * Define a global instance of the JSON factory.
	 */
	public static final JsonFactory JSON_FACTORY = new JacksonFactory();

	/**
	 * Define a global instance of a Youtube object, which will be used to make
	 * YouTube Data API requests. Set your developer key from the Google
	 * Developers Console for non-authenticated requests. See:
	 * https://console.developers.google.com/
	 */
	private YouTube youtube;

	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try {
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(
					chercher(request.getParameter("apiKey"),
							request.getParameter("requete")));
		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().println(e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recherche sur youtube
	 *
	 * @param args
	 *            API_KEY, request
	 * 
	 */
	public static void main(String[] args) {
		RechercheYoutube rechercheYoutube = new RechercheYoutube();
		System.out.println(rechercheYoutube.chercher(args[0], args[1]));
		System.exit(0);
	}

	private String chercher(String apiKey, String requete) {
		String resultat = "";
		try {
			// Define the API request for retrieving search results.
			YouTube.Search.List search = youtube.search().list("id,snippet");

			search.setKey(apiKey);
			search.setQ(requete);

			// Restrict the search results to only include videos. See:
			// https://developers.google.com/youtube/v3/docs/search/list#type
			search.setType("video");

			// To increase efficiency, only retrieve the fields that the
			// application uses.
			// search.setFields("items(id/videoId)");
			search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);

			// Call the API and print results.
			SearchListResponse searchResponse = search.execute();
			if (searchResponse.getItems().size() > 0) {
				SearchResult searchResult = searchResponse.getItems().get(0);
				resultat = "<resultat><videoId>http://www.youtube.com/watch?v="
						+ searchResult.getId().getVideoId()
						+ "</videoId><title>"
						+ searchResult.getSnippet().getTitle()
								.replaceAll("[&<>]", " ")
						+ "</title></resultat>";
			} else {
				System.err.println("Pas de résultat");
			}
		} catch (GoogleJsonResponseException e) {
			System.err.println("There was a service error: "
					+ e.getDetails().getCode() + " : "
					+ e.getDetails().getMessage());
		} catch (IOException e) {
			System.err.println("There was an IO error: " + e.getCause() + " : "
					+ e.getMessage());
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return resultat;
	}

	public RechercheYoutube() {

		// This object is used to make YouTube Data API requests. The last
		// argument is required, but since we don't need anything
		// initialized when the HttpRequest is initialized, we override
		// the interface and provide a no-op function.
		youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				new HttpRequestInitializer() {
					@Override
					public void initialize(HttpRequest request)
							throws IOException {
					}
				}).setApplicationName("carl").build();
	}
}
