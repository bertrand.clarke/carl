import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

/**
 * <div>Texte à traduire paramètre</div>
 * 
 * @author bcdlog
 *
 */
public class Traduction extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try {
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(
					traduire(request.getParameter("clientId"),
							request.getParameter("clientSecret"),
							request.getParameter("fromLanguage"),
							request.getParameter("toLanguage"),
							request.getParameter("text")));
		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().println(e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	public Traduction() {

	}

	public String traduire(String clientId, String clientSecret,
			String fromLanguage, String toLanguage, String text)
			throws Exception {
		Translate.setClientId(clientId);
		Translate.setClientSecret(clientSecret);

		String translatedText = null;
		if (toLanguage.equals("espagnol")) {
			translatedText = Translate.execute(text, Language.FRENCH,
					Language.SPANISH);
		} else if (toLanguage.equals("anglais")) {
			translatedText = Translate.execute(text, Language.FRENCH,
					Language.ENGLISH);
		}
		return translatedText;
	}

	public static void main(String[] args)
			throws FailingHttpStatusCodeException, MalformedURLException,
			Exception {
		Traduction traduction = new Traduction();
		System.out.println(traduction.traduire(args[0], args[1], args[2],
				args[3], args[4]));
		System.exit(0);
	}
}
