import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlListItem;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * <div>Paramètres</div> <div>Numéro du département sur 2
 * caractères</div><div>temps d'attente du chargement de la page en ms</div>
 * Picto pris de
 * http://vigilance.meteofrance.com/html/vigilance/guideVigilance/images/guide/
 * 
 * @author bcdlog
 *
 */
public class RequeteDefinition extends HttpServlet {

	private static final long serialVersionUID = 1L;

	static String URL = "http://fr.wiktionary.org/wiki/";

	protected HtmlPage page;

	protected WebClient webClient = new WebClient();

	protected StringBuffer reponse = new StringBuffer();

	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try {
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(
					analysePage(request.getParameter("departement")));
		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().println(e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	public RequeteDefinition() {

	}

	private String analysePage(String mot)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException {
		webClient = new WebClient();
		HtmlPage htmlPage = webClient.getPage(URL + mot);
		// webClient.waitForBackgroundJavaScript(timeout);
		StringBuffer stringBuffer = new StringBuffer();
		for (Object element : htmlPage.getByXPath("//ol/li")) {
			HtmlListItem htmlListItem = (HtmlListItem) element;
			for (HtmlElement sousElement : htmlListItem.getChildElements()) {
				if (sousElement.getNodeName().equals("ul")
						|| sousElement.getNodeName().equals("ol")) {
					htmlListItem.removeChild(sousElement);
				} else {
					System.out.println(sousElement.asXml());
				}
			}
			System.out.println(htmlListItem.asText());
		}

		webClient.closeAllWindows();
		return stringBuffer.toString();
	}

	public static void main(String[] args)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException {
		RequeteDefinition requeteDefinition = new RequeteDefinition();
		System.out.println(requeteDefinition.analysePage(args[0]));
		System.exit(0);
	}

}
