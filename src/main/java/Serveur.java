import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class Serveur {
	public static void main(final String[] args) throws Exception {
		final Server server = new Server(8088);

		final ServletContextHandler context = new ServletContextHandler(
				ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);
		context.addServlet(new ServletHolder(new RequeteDefinition()),
				"/definition");
		context.addServlet(new ServletHolder(new Traduction()), "/traduction");
		context.addServlet(new ServletHolder(new RechercheYoutube()),
				"/youtube");
		context.addServlet(new ServletHolder(new VigilanceMeteo()), "/meteo");
		server.start();
		server.join();
	}
}
