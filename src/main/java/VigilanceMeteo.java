import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomAttr;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.xml.XmlPage;

/**
 * <div>Paramètres</div> <div>Numéro du département sur 2
 * caractères</div><div>temps d'attente du chargement de la page en ms</div>
 * Picto pris de
 * http://vigilance.meteofrance.com/html/vigilance/guideVigilance/images/guide/
 * 
 * @author bcdlog
 *
 */
public class VigilanceMeteo extends HttpServlet {

	private static final long serialVersionUID = 1L;

	static String URL = "http://vigilance.meteofrance.com/data/NXFR33_LFPW_.xml?43837";

	static String PREFIX = "html/vigilance/guideVigilance/images/guide/pictoM_";

	protected WebClient webClient = new WebClient();

	protected StringBuffer reponse = new StringBuffer();

	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try {
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(
					analysePage(request.getParameter("departement")));
		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().println(e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	public VigilanceMeteo() {

	}

	private String analysePage(String departement)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException {
		webClient = new WebClient();
		XmlPage xmlPage = webClient.getPage(URL);
		// webClient.waitForBackgroundJavaScript(timeout);
		StringBuffer stringBuffer = new StringBuffer();
		for (int couleur = 2; couleur <= 4; ++couleur) {
			for (Object object : xmlPage.getByXPath("//DV[@dep='" + departement
					+ "' or @dep='" + departement + "10'][@coul='" + couleur
					+ "']/risque/@val")) {
				DomAttr domAttr = (DomAttr) object;
				stringBuffer.append(analyse(
						Integer.parseInt(domAttr.getValue()), couleur));
			}
		}
		webClient.closeAllWindows();
		return stringBuffer.toString();
	}

	public static void main(String[] args)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException {
		VigilanceMeteo vigilanceMeteo = new VigilanceMeteo();
		System.out.println(vigilanceMeteo.analysePage(args[0]));
		System.exit(0);
	}

	private String analyse(int risque, int couleur)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException {
		reponse = new StringBuffer("Alerte de niveau ");
		String risqueTexte = null;
		String couleurTexte = null;
		switch (couleur) {
		case 2:
			reponse.append("jaune ");
			break;
		case 3:
			reponse.append("orange ");
			couleurTexte = "or";
			break;
		case 4:
			reponse.append("rouge ");
			couleurTexte = "red";
			break;
		default:
			System.out.println("Erreur couleur inconnue");
		}

		switch (risque) {
		case 1:
			reponse.append("vent violent");
			risqueTexte = "vent";
			break;
		case 2:
			reponse.append("pluie inondation");
			risqueTexte = "precipit";
			break;
		case 3:
			reponse.append("orage");
			risqueTexte = "orage";
			break;
		case 4:
			reponse.append("crue innondation");
			risqueTexte = "inondation";
			break;
		case 5:
			reponse.append("neige verglas");
			risqueTexte = "neige";
			break;
		case 6:
			reponse.append("cannicule");
			risqueTexte = "chaud";
			break;
		case 7:
			reponse.append("grand froid");
			risqueTexte = "froid";
			break;
		case 8:
			reponse.append("avalanche");
			risqueTexte = "avalanche";
			break;
		case 9:
			reponse.append("vague submersion");
			risqueTexte = "vagues";
			break;
		default:
			System.out.println("Erreur risque inconnu");
		}
		reponse.append(". ");
		if (couleur == 4) {
			ajouterConseils(risqueTexte, couleurTexte);
		}
		return reponse.toString();
	}

	private void ajouterConseils(String risque, String couleur)
			throws FailingHttpStatusCodeException, MalformedURLException,
			IOException {
		if (risque.equals("crue")) {
			risque = "inondation";
		} else if (risque.equals("innond")) {
			risque = "precipit";
		} else if (risque.equals("vague")) {
			risque = "vagues";
		}
		HtmlPage pageConseils = webClient
				.getPage("http://vigilance.meteofrance.com/html/vigilance/guideVigilance/cc_"
						+ risque + "_" + couleur + ".html");
		final DomElement titre = (DomElement) pageConseils.getByXPath(
				"//h2[text()='Conseils de comportement']").get(0);
		reponse.append(" ").append(
				titre.getParentNode().getParentNode().asText());
	}
}
